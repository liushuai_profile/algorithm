# -*- coding: UTF-8 -*-

# https://docs.google.com/document/d/1DC282EyVfgN8i-jl5hGXOi_Divkzdv9q1WRFXfQlo2o/edit


class Node(object):
    def __init__(self, val=None, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution(object):
    def __init__(self):
        self.stack, self.res = [], []

    def preorderTraversal(self, root):  # 先序遍历
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if not root: return None
        # return [root.val] + self.preorderTraversal(root.left) + self.preorderTraversal(root.right)
        p = root
        while p or self.stack:
            if p:
                self.res.append(p.val)
                self.stack.append(p)
                p = p.left
            else:
                p = self.stack.pop()
                p = p.right
        return self.res

    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if not root: return []
        # return self.inorderTraversal(root.left) + [root.val] + self.inorderTraversal(root.right)
        p = root
        while p or self.stack:
            if p:
                self.stack.append(p)
                p = p.left
            else:
                p = self.stack.pop()
                self.res.append(p.val)
                p = p.right
        return self.res

    def postorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if not root: return []
        # return  self.postorderTraversal(root.left) + self.postorderTraversal(root.right) + [root.val]
        p, r = root, None
        while p or self.stack:
            if p:
                self.stack.append(p)
                p = p.left
            else:
                p = self.stack[-1]
                if not p.right or p.right == r:
                    self.stack.pop()
                    self.res.append(p.val)
                    r = p
                    p = None
                else:
                    p = p.right
        return self.res

    def levelOrderBranch(self, root):   # 分行打印
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root: return []
        queue = [root]
        while queue:
            cur_level = []
            for _ in range(len(queue)):
                t = queue.pop(0)
                cur_level.append(t.val)
                if t.left: queue.append(t.left)
                if t.right: queue.append(t.right)
            self.res.append(cur_level)
        return self.res

    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root: return []
        queue = [root]
        while queue:
            t = queue.pop(0)
            self.res.append(t.val)
            if t.left: queue.append(t.left)
            if t.right: queue.append(t.right)
        return self.res


if __name__=='__main__':
    root = Node('D', Node('B', Node('A'), Node('C')), Node('E', right=Node('G', Node('F'))))
    print Solution().preorderTraversal(root)
    print Solution().inorderTraversal(root)
    print Solution().postorderTraversal(root)
    print Solution().levelOrder(root)
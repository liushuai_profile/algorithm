# -*- coding: UTF-8 -*-


class Solution(object):
    # # https://www.jianshu.com/p/0a597adfc1e8
    def __init__(self):
        self.graph = {
            "A": ["B", "C"],
            "B": ["A", "C", "D"],
            "C": ["A", "B", "D", "E"],
            "D": ["B", "C", "E", "F"],
            "E": ["C", "D"],
            "F": ["D"]
        }
        self.stack, self.queue, self.res, self.seen = [], [], [], set()

    def DFS(self, s):
        self.stack.append(s)  # 深度优先搜索：栈
        self.seen.add(s)

        while len(self.stack) > 0:
            v = self.stack.pop()
            for w in self.graph[v]:
                if w not in self.seen:
                    self.stack.append(w)
                    self.seen.add(w)
            self.res.append(v)
        return self.res

    def BFS(self, s):
        self.queue.append(s)  # 广度优先搜索：队列
        self.seen.add(s)

        while len(self.queue) > 0:
            v = self.queue.pop(0)
            for w in self.graph[v]:
                if w not in self.seen:
                    self.queue.append(w)
                    self.seen.add(w)
            self.res.append(v)
        return self.res


if __name__ == '__main__':
    a = Solution()
    print a.BFS("A")
    print a.DFS("A")

# -*- coding: UTF-8 -*-
from random import randint


class Solution(object):  # leetcode 75   912
    def sortColors(self, nums):
        """
        https://blog.csdn.net/bjweimengshu/article/details/103573147
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        return self.bubble_sort(nums)
        # return self.select_sort(nums)
        # return self.insert_sort(nums)
        # return self.quick_sort(nums)
        # return self.heap_sort(nums)
        # return self.shell_sort(nums)
        # return self.merge_sort(nums)
        # return self.bucket_sort(nums)
        # return self.counting_sort(nums)
        # return self.radix_sort(nums)

    def bubble_sort(self, a):
        length = len(a)
        for i in range(0, length):
            flag = False
            for j in range(0, length-1 - i):
                if a[j] > a[j + 1]:
                    a[j], a[j + 1] = a[j + 1], a[j]
                    flag = True
            if flag is False:
                break
        return a

    def select_sort(self, a):
        length = len(a)
        for i in range(0, length - 1):
            min = i
            for j in range(i + 1, length):
                if a[j] < a[min]:
                    min = j
            if min != i:
                a[min], a[i] = a[i], a[min]
        return a

    def insert_sort(self, a):
        length = len(a)
        for i in range(1, length):   # 第一个已经排好，故从第二个位置开始循环
            flag = a[i]  # 该趟要排的元素
            j = i - 1  # j有序区的最后一元素
            while flag < a[j] and j >= 0:
                a[j + 1] = a[j]
                j -= 1
            a[j + 1] = flag
        return a

    def quick_sort(self, nums):
        """
        提交记录：submissions/detail/43920192/
        :param nums:
        :return: nums
        """
        def partition(nums, low, high):
            i, j = low, high
            flag = nums[low]
            while i < j:         # 选择与基准相反的方向开始
                while i < j and nums[j] >= flag: j -= 1
                while i < j and nums[i] <= flag: i += 1
                if i < j: nums[i], nums[j] = nums[j], nums[i]
            nums[low] = nums[i]
            nums[i] = flag       # base归位
            return i             # 返回最终的基准位置

        def q_sort(nums, low, high):
            if low < high:
                pos = partition(nums, low, high)
                q_sort(nums, low, pos - 1)
                q_sort(nums, pos + 1, high)
            return nums
            # -------------------------------------
        return q_sort(nums, 0, len(nums) - 1)

    def heap_sort(self, nums):
        """
        提交记录：submissions/detail/43920145/
        :param nums:
        :return: nums
        """
        def sift(nums, length, dad):
            flag = 2 * dad + 1
            while flag < length:
                if flag + 1 < length and nums[flag] < nums[flag + 1]:
                    flag += 1
                if nums[dad] >= nums[flag]:
                    break
                nums[dad], nums[flag] = nums[flag], nums[dad]
                dad = flag            # large位置的堆状态可能被破坏,继续调整
                flag = 2 * dad + 1

        def h_sort(nums):
            if not nums: return nums
            # ----创建堆-------------------------
            length = len(nums)
            for i in range(length >> 1 - 1, -1, -1):
                sift(nums, length, i)
            for i in range(length - 1, 0, -1): # n-1：最后一个节点开始排序
                nums[0], nums[i] = nums[i], nums[0]    # 堆排序：最值下降
                sift(nums, i, 0)                       # 调整堆：最值上升
            return nums
        # -------------------------------------
        return h_sort(nums)

    def shell_sort(self, nums):
        """
        # 提交记录：submissions/detail/43920085/
        :param nums:
        :return: nums
        """
        d = len(nums) >> 1
        while d > 0:
            for i in range(d, len(nums)):
                flag = nums[i]
                j = i - d
                while flag < nums[j] and j >= 0:
                    nums[j + d] = nums[j]
                    j -= d
                nums[j + d] = flag
            d /= 2
        return nums

    def merge_sort(self, nums):
        """
        # 提交记录：submissions/detail/43920030/
        :param nums:
        :return:
        """
        def merge(left, right):
            i, j = 0, 0
            rtl = []
            while i < len(left) and j < len(right):
                if left[i] <= right[j]:
                    rtl.append(left[i])
                    i += 1
                else:
                    rtl.append(right[j])
                    j += 1
            if len(left) > 0: rtl += left[i:]
            if len(right) > 0: rtl += right[j:]
            return rtl

        def m_sort(nums):
            if len(nums) <= 1:
                return nums
            mid = len(nums) / 2
            left, right = m_sort(nums[:mid]), m_sort(nums[mid:])
            return merge(left, right)
        # -------------------------------------
        return m_sort(nums)

    def counting_sort(self, nums):
        """
        桶：每个桶只存储单一键值
        提交记录：submissions/detail/43919796/
        :param nums:
        :return:
        """
        # 1 计算差值
        Max, Min = max(nums), min(nums)
        d = Max - Min
        if len(nums) <= 1 or d == 0:
            return nums
        # 2 创建统计数组，并统计对应元素的个数
        counts = [0] * (d + 1)
        for num in nums:
            counts[num - Min] += 1
        # 3 变形，后面元素等于前面元素之和
        for i in range(1, len(counts)):
            counts[i] += counts[i - 1]
        # 4 倒序从统计数组找出正确位置，
        rlt = [None] * len(nums)
        for i in range(len(nums)-1, -1, -1):
            index = nums[i] - Min
            rlt[counts[index]-1] = nums[i]
            counts[index] -= 1
        return rlt

    def bucket_sort(self, nums):
        """
        桶：每个桶存储一定范围的数值
        提交记录 : submissions/detail/43919630/
        :param nums:
        :return:
        """
        # 1 计算差值
        Max, Min = max(nums), min(nums)
        d = Max - Min
        if len(nums) <= 1 or d == 0:
            return nums
        # 2 初始化桶
        bucketNum = len(nums)
        buckets = [[] for _ in range(bucketNum)]
        # 3 遍历原始数组，将每个元素放入桶中
        for num in nums:
            index = (num - Min) / d * (bucketNum - 1)
            buckets[(int)(index)].append(num)
        # 4 对每个桶内部进行排序并输出全部元素
        rlt = []
        for bucket in buckets:
            bucket.sort()
            rlt.extend(bucket)
        return rlt

    def radix_sort(self, nums):
        """
        桶：根据键值的每位数字来分配桶
        提交记录：submissions/detail/43919578/
        :param nums:
        :return:
        """
        # 统计最大数的位数
        # max_digit = 1
        # while max(nums)/(10**max_digit)>0:
        #     max_digit += 1
        max_digit = len(str(max(nums)))
        for digit in range(max_digit):
            # 因为每一位数字都是0~9，故建立10个桶
            buckets = [[] for _ in range(10)]
            for num in nums:
                # 得到每位基数，并将对应元素入桶
                index = (num/10 ** digit) % 10
                buckets[index].append(num)
            coll = []
            for bucket in buckets:
                if bucket:
                    for num in bucket:
                        coll.append(num)
                else:
                    pass
            nums = coll
        return nums


if __name__ == '__main__':
    nums = [randint(1, 1000) for i in range(20)]
    print nums

    nums_sort= Solution().sortColors(nums)
    print nums_sort

    if nums_sort == sorted(nums):
        print 'yes'
    else:
        print 'no'

# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def reverseList(self, head):
        """
        https://leetcode-cn.com/submissions/detail/42283682/
        :type head: ListNode
        :rtype: ListNode
        """
        if not head: return head
        cur, pre = head, None  # 当前, 前驱
        while cur:
            next = cur.next  # 后继
            cur.next = pre  # -- 反转--
            pre, cur = cur, next  # --前移--
        return pre

    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        nhead = ListNode('c')
        nhead.next = head
        pre_m = nhead
        # 来到m节点的前一个节点
        for _ in range(m - 1):  pre_m = pre_m.next

        # 从m节点前一个节点，再来到n节点
        p_n = pre_m
        for _ in range(n - (m - 1)): p_n = p_n.next

        # 切出一个m到n的子链表，断开后继之前，先记住后继
        p_m, pre_m.next = pre_m.next, None
        next_n, p_n.next = p_n.next, None

        # 反转子链表
        self.reverseList(p_m)
        # 接回到原链表中
        pre_m.next = p_n
        p_m.next = next_n

        return nhead.next


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode('A', ListNode('B', ListNode('C', ListNode('D', ListNode('E')))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    new_head = Solution().reverseBetween(head, 2, 4)
    print print_list(new_head)      # ['A', 'D', 'C', 'B', 'E']


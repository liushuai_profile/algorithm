# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def isPalindrome(self, head):
        """
        https: // leetcode - cn.com / submissions / detail / 42385823 /
        :type head: ListNode
        :rtype: ListNode
        """

        if not head:
            return True
        fast = slow = head
        # 快指针指向下两个，这样遍历之后，slow只走到中间节点
        while fast and fast.next :
            slow = slow.next
            fast = fast.next.next
        # 将中间节点之后的链表反转
        cur, pre = slow, None
        while cur:
            next = cur.next
            cur.next = pre
            pre, cur = cur, next
        # 重新以head开始比较反转后的链表
        while pre:
            if pre.val != head.val:
                return False
            pre = pre.next
            head = head.next
        return True


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode('A', ListNode('B', ListNode('C', ListNode('B', ListNode('A')))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    print Solution().isPalindrome(head)

# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def middleNode(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head.next is None:
            return head
        pre,cur = head, head
        while cur and cur.next:
            pre = pre.next
            cur = cur.next.next
        return pre


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode('A', ListNode('B', ListNode('C', ListNode('D', ListNode('E')))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    print Solution().middleNode(head).val


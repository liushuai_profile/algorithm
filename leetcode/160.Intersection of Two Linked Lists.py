# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def len_list(self, head):
        _len = 0
        while head:
            _len += 1
            head = head.next
        return _len

    def getIntersectionNode(self, headA, headB):
        """
        https://leetcode-cn.com/submissions/detail/42301279/
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        if not headA or not headB: return None
        lenA = self.len_list(headA)
        lenB = self.len_list(headB)
        diff = abs(lenA - lenB)
        # p指向长链表，q指向短链表
        if lenA >= lenB: p, q = headA, headB
        else: p, q = headB, headA
        # 长的先走diff步
        for i in range(diff): p = p.next
        # 两者再一起走，相遇则相交
        while p and q and p!= q:
            p, q = p.next, q.next
        return p




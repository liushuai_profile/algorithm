# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next =None):
        self.val = x
        self.next = next


class Solution(object):
    def hasCycle(self, head):
        """
        https://leetcode-cn.com/submissions/detail/42296082/
        :type head: ListNode
        :rtype: bool
        """
        if not head: return False
        pre, cur = head, head
        while cur.next and cur.next.next:
            pre, cur = pre.next, cur.next.next
            if pre == cur:
                return True
        return False


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head_A = ListNode('A')
    head_B = ListNode('B', head_A)
    head_C = ListNode('C', head_B)
    head_D = ListNode('D', head_B)
    head_E = ListNode('E', head_D)




    print print_list(head_E)          # ['A', 'B', 'C', 'D', 'E']

    print Solution().hasCycle(head_E)      # ['A', 'D', 'C', 'B', 'E']

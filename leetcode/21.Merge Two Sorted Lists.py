# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        https://leetcode-cn.com/submissions/detail/42302956/
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        nhead = ListNode( 'c')
        cur = nhead
        while l1 and l2:
            if l1.val<l2.val:
                cur.next,l1 = l1,l1.next
            else:
                cur.next,l2 = l2,l2.next
            cur = cur.next
        if l1: cur.next = l1
        else:  cur.next = l2
        return nhead.next


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    headA = ListNode(1, ListNode(3, ListNode(4, ListNode(7, ListNode(9)))))
    headB = ListNode(2, ListNode(5, ListNode(6, ListNode(8, ListNode(9)))))


    new_head = Solution().mergeTwoLists(headA, headB)
    print print_list(new_head)      # ['A', 'D', 'C', 'B', 'E']

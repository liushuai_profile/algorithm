# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def deleteDuplicates(self, head):
        """
        https://leetcode-cn.com/submissions/detail/42288149/
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next: return head
        nhead = ListNode('c') # 创建一个头结点
        nhead.next = head
        pre, cur= nhead, head     # pre慢指针，cur快指针
        while cur and cur.next:
            if cur.val == cur.next.val:
                val = cur.val  # 记住这个重复值
                while cur and val == cur.val:
                    cur = cur.next # 快指针负责跳过所有重复节点
                pre.next = cur     # 慢指针负责拼接
            else:
                pre, cur = cur, cur.next # 两指针一起后移
        return nhead.next


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode(3, ListNode(3, ListNode(3, ListNode(4, ListNode(5)))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    new_head = Solution().deleteDuplicates(head)
    print print_list(new_head)      # ['A', 'D', 'C', 'B', 'E']

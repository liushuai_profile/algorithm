# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def deleteDuplicates(self, head):
        """
        https://leetcode-cn.com/submissions/detail/42288110/
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next: return head
        pre, cur = head, head.next # p慢指针，q快指针
        while cur:  # 遍历快指针
            if pre.val == cur.val: #前后值重复时
                cur=cur.next # 快指针后移
            else:              # 不重复时
                pre.next = cur # 快慢指针链起来，删除重复值
                pre, cur = cur, cur.next #快慢指针都后移
        pre.next = None # 末尾元素也为重复值时，q此时为空
        return head


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode(3, ListNode(3, ListNode(3, ListNode(4, ListNode(5)))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    new_head = Solution().deleteDuplicates(head)
    print print_list(new_head)      # ['A', 'D', 'C', 'B', 'E']

# -*- coding: UTF-8 -*-
class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution:
    # 返回从尾部到头部的列表值序列，例如[1,2,3]
    def printListFromTailToHead(self, listNode):
    # write code here
        if listNode is None:
            return []
        # res = []
        # if listNode.next!=[]:
        #     res = self.printListFromTailToHead(listNode.next)
        #     res.append(listNode.val)
        # return res
        return self.printListFromTailToHead(listNode.next) + [listNode.val]

    def printListFromTailToHead_1(self, head):
        # write code here
        if not head:
            return []
        stack, res = [],[]
        while head:
            stack.append(head.val)
            head = head.next
        # while stack:
        #     res.append(stack.pop())
        # return res
        return stack[::-1]


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode('A', ListNode('B', ListNode('C')))
    print print_list(head)         # ['A', 'B', 'C']

    print Solution().printListFromTailToHead_1(head)

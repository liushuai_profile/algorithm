# -*- coding: UTF-8 -*-
class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def reverseList(self, head):
        """
        迭代法
        https://leetcode-cn.com/submissions/detail/42283743/
        :type head: ListNode
        :rtype: ListNode
        """
        if not head:
            return head
        cur, pre = head, None          # 当前, 前驱
        while cur:
            next = cur.next            # 后继
            cur.next = pre             # --反转--
            pre, cur = cur, next       # --前移--
        return pre

    def reverse_list(self, head):
        """
        递归法
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next:
            return head
        new_head = self.reverse_list(head.next)
        head.next.next = head
        head.next = None
        return new_head


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode('A', ListNode('B', ListNode('C')))
    print print_list(head)         # ['A', 'B', 'C']

    new_head = Solution().reverseList(head)
    print print_list(new_head)     # ['C', 'B', 'A']

    new_head_re = Solution().reverse_list(new_head)
    print print_list(new_head_re)  # ['A', 'B', 'C']
# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def removeNthFromEnd(self, head, n):
        """
        https://leetcode-cn.com/submissions/detail/42283607/
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        nhead = ListNode('c')  # 建立虚拟头节点
        nhead.next = head

        slow,fast = nhead,nhead
        for _ in range(n):
            if fast:
                fast = fast.next
            else:
                 return None
        while fast.next:
            fast, slow = fast.next, slow.next
        slow.next = slow.next.next
        return nhead.next


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode('A', ListNode('B', ListNode('C', ListNode('D', ListNode('E')))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    new_head = Solution().removeNthFromEnd(head, 5)
    print print_list(new_head)      # ['A', 'D', 'C', 'B', 'E']

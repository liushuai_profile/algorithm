# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def removeElements(self, head, val):
        """
        https://leetcode-cn.com/submissions/detail/42283074/
        :type head: ListNode
        :type val: int
        :rtype: ListNode
        """
        nhead = ListNode('c')
        nhead.next = head
        cur = nhead
        while cur.next:
            if cur.next.val == val:
                cur.next = cur.next.next
            else:
                cur = cur.next
        return nhead.next


def print_list(head):
    res = []
    if not head: return None
    while head:
        res.append(head.val)
        head = head.next
    return res


if __name__ == '__main__':
    head = ListNode(3, ListNode(3, ListNode(3, ListNode(4, ListNode(5)))))
    print print_list(head)          # ['A', 'B', 'C', 'D', 'E']

    new_head = Solution().removeElements(head, 3)
    print print_list(new_head)      # ['A', 'D', 'C', 'B', 'E']

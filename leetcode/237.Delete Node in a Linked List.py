# -*- coding: UTF-8 -*-


class ListNode(object):
    def __init__(self, x, next = None):
        self.val = x
        self.next = next


class Solution(object):
    def deleteNode(self, node):
        """
        https://leetcode-cn.com/submissions/detail/42070131/
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        # if node == None:
        #     return node
        #
        # if node.next == None:
        #     return None

        if node.next != None:
            nextNode = node.next
            node.val = nextNode.val
            node.next = nextNode.next
        # else:
        #     cur = node
        #     while cur.next != node:
        #         cur = cur.next
        #     cur.next = None




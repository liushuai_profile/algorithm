class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        def binarySearch(nums, target):
            l, r = 0, len(nums) - 1
            while l <= r:
                mid = (l + r) >> 1
                if nums[mid] == target:
                    return mid
                elif nums[mid] < target:
                    l = mid + 1
                else:
                    r = mid - 1
            return -1

        return binarySearch(nums, target)


if __name__ == '__main__':
    print Solution().search([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3)